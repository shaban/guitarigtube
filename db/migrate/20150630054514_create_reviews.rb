class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.integer :model_id
      t.string  :name
      t.text    :descriptions
      t.text    :review
      t.decimal :rating
      t.timestamps
    end
  end
end
