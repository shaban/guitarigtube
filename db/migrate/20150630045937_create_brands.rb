class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.integer :cateogry_id
      t.integer :sub_category_id
      t.string  :name
      t.text    :descriptions
      t.timestamps
    end
  end
end
