class Brand < ActiveRecord::Base
  belongs_to :sub_category
  has_many :models
end
